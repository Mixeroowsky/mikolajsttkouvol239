﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Vector3 pos, offset, circleCenter;  
    
    void Start()
    {
        circleCenter = transform.position + new Vector3(0, 2, 0);
    }

    // Update is called once per frame
    void Update()
    {        
        
        float radius = 2.1f; // this is the range you want the player to move without restriction
        float dist = Vector3.Distance(transform.position, circleCenter); // the distance from player current position to the circleCenter

        if (dist > radius)
        {
            Vector3 fromOrigintoObject = transform.position - circleCenter;
            fromOrigintoObject *= radius / dist;
            transform.position = circleCenter + fromOrigintoObject;
        }
    }
    private void OnMouseDown()
    {
        pos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, pos.z));
    }
    private void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, pos.z);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        transform.position = cursorPosition;
    }
}
