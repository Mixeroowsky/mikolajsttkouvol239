﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField]
    GameObject tower;
    Queue<GameObject> availableObjects = new Queue<GameObject>();
    public static ObjectPooler Instance { get; private set; }
    void Awake()
    {
        Instance = this;
        GrowPool();
    }
    void GrowPool()
    {
        for (int i = 0; i < 150; i++)
        {
            var instanceToAdd = Instantiate(tower);
            instanceToAdd.transform.SetParent(transform);
            AddToPool(instanceToAdd);
        }        
    }
    public GameObject GetFromPool()
    {
        if (availableObjects.Count == 0)
        {
            GrowPool();
        }
        var instance = availableObjects.Dequeue();
        instance.SetActive(true);
        return instance;
    }
    public void AddToPool(GameObject instance)
    {
        instance.SetActive(false);
        availableObjects.Enqueue(instance);
    }
}
