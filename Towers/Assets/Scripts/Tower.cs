﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject shot;
    public Transform shotZone;
    float rotation;
    GameObject[] towers;
    public static int towersNumber;
    Color colorWait = Color.white;
    Color colorShoot = Color.red;
    Renderer renderer;
    public static bool isOver100 = false;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        StartCoroutine(Wait());
        
    }

    // Update is called once per frame
    void Update()
    {
        towers = GameObject.FindGameObjectsWithTag("Tower");
        towersNumber = towers.Length;
        if(towersNumber >= 100)
        {
            isOver100 = true;
        }
        if(isOver100)
        {
            Over100();
        }
    }
    IEnumerator Rotate()
    {
        renderer.material.color = colorShoot;
        for (int i = 0; i < 12; i++)
        {
            yield return new WaitForSeconds(.5f);
            
            rotation += Random.Range(15, 45);
            Vector3 rotationVector = new Vector3(0, 0, rotation);
            transform.rotation = Quaternion.Euler(rotationVector);        
            Instantiate(shot,shotZone.position,shotZone.rotation);
        }
        renderer.material.color = colorWait;
        
    }
    IEnumerator Wait()
    {
        renderer.material.color = colorWait;
        yield return new WaitForSeconds(6f);
        StartCoroutine(Rotate());
    }
    void Over100()
    {
        StartCoroutine(Rotate());
        isOver100 = false;
    }
}
