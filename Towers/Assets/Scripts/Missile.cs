﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public GameObject tower;
    Rigidbody2D rb;
    public float speed = 4f, timer = 0;
    float distance;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;
        distance = Random.Range(1, 4);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.deltaTime;
        if(distance<=timer && Tower.towersNumber < 100 && !Tower.isOver100)
        {
            var enemyClone = ObjectPooler.Instance.GetFromPool();
            enemyClone.transform.position = transform.position;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Tower")
        {
            ObjectPooler.Instance.AddToPool(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
